"""
Vorislik va Polimorfizm
"""

class Shaxs:
    def __init__(self, ism, familya, passport, tyil):
        self.ism = ism
        self.familya = familya
        self.passport = passport
        self.tyil = tyil

    def get_info(self):
        info = f"{self.ism} {self.familya}"
        info += f"Passport: {self.passport}, {self.tyil}-yilda tug'ilgan"
        return info

    def get_age(self, yil):
        return yil - self.tyil


class Talaba(Shaxs):
    def __init__(self, ism, familya, passport, tyil, idraqam, manzil):
        super().__init__(ism, familya, passport, tyil)
        self.idraqam = idraqam
        self.bosqich = 1
        self.manzil = manzil

    def get_id(self):
        return self.idraqam

    def get_bosqich(self):
        return self.bosqich

    def get_info(self):
        info = f"{self.ism} {self.familya}. "
        info += f"{self.get_bosqich()}-bosqich, ID raqam: {self.get_id()}"
        return info


class Manzil:
    def __init__(self, uy, kocha, tuman, viloyat):
        self.uy = uy
        self.kocha = kocha
        self.tuman = tuman
        self.viloyat = viloyat

    def get_info(self):
        manzil = f"{self.viloyat} viloyati, {self.tuman} tumani, "
        manzil += f"{self.kocha} ko'chasi, {self.uy}-uy"
        return manzil


talaba1_manzil = Manzil(12, "Olmazor", "Bog'bon", "Samarqand")
talaba1 = Talaba("Alijon", "Valiyev", "FF112233", 2001, "N0001112", manzil=talaba1_manzil)
print(talaba1_manzil.get_info())
print(talaba1.get_info())
