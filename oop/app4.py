"""
DUNDER METHODS
"""


class Avto:
    __num_avto = 0  # classga tegishli o'zgaruvchi

    def __init__(self, make, model, rang, yil, narx, km=0):
        self.make = make
        self.model = model
        self.rang = rang
        self.yil = yil
        self.narx = narx
        Avto.__num_avto += 1

    def __str__(self):  # toString function in Java
        return f"Avto: {self.make} {self.model}"

    # SUGGESTED
    def __repr__(self):  # toString function in Java, same as above
        return f"Avto: {self.make} {self.model}"

    # COMPARISON OPERATORS
    ##################################
    def __eq__(self, other):
        return self.narx == other.narx

    def __lt__(self, other):
        return self.narx < other.narx

    def __le__(self, other):
        return self.narx <= other.narx
    ##################################


class AvtoSalon:
    def __init__(self, name):
        self.name = name
        self.avtolar = []

    def __repr__(self):
        return f"{self.name} avtosaloni"

    # Array kabi murojaat qilish imkonini beradi
    ############################################
    def __getitem__(self, index):
        return self.avtolar[index]

    def __setitem__(self, index, qiymat):
        self.avtolar[index] = qiymat
    ############################################

    def add_avto(self, *qiymat):
        for avto in qiymat:
            if isinstance(avto, Avto):
                self.avtolar.append(avto)
            else:
                print("Avto kiriting")


salon1 = AvtoSalon("MaxAvto")

avto1 = Avto("GM", "Malibu", "Qora", 2020, 40000)
avto2 = Avto("GM", "Gentra", "Oq", 2020, 20000)
avto3 = Avto("Toyota", "Carolla", "Silver", 2018, 45000)
salon1.add_avto(avto1, avto2, avto3)
print(salon1[1])
salon1[1] = Avto("BMW", "x7", "Qora", 2019, 70000)
print(salon1[:])